Source: sambamba
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-dlang,
               libhts-dev,
               meson,
               pkg-config,
               python3,
               shunit2 <!nocheck>,
               zlib1g-dev,
               ldc,
# used by libhts and explicitly referenced in build instruction
               liblz4-dev,
               libcurl4-gnutls-dev | libcurl-dev,
               libssl-dev,
               libbz2-dev,
               libdeflate-dev,
               liblzma-dev,
               debhelper
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/sambamba
Vcs-Git: https://salsa.debian.org/med-team/sambamba.git
Homepage: https://github.com/lomereiter/sambamba
Rules-Requires-Root: no

Package: sambamba
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: tools for working with SAM/BAM data
 Sambamba positions itself as a performant alternative
 to samtools and provides tools for
  * Powerful filtering with sambamba view --filter
  * Picard-like SAM header merging in the merge tool
  * Optional for operations on whole BAMs
  * Fast copying of a region to a new file with the slice tool
  * Duplicate marking/removal, using the Picard criteria
